package JavaRMIServidor.src;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Server {

    private Registry servicoNomes;
    private ServImpl serv;

    Server() throws RemoteException {
        servicoNomes = LocateRegistry.createRegistry(2020);
        serv = new ServImpl();
        servicoNomes.rebind("Transfers", serv);
    }

    public static void main(String[] args) {
        try {
            new Server();
            System.out.println("SERVER ATIVO");
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

}
