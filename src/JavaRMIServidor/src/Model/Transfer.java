package JavaRMIServidor.src.Model;

import JavaRMICliente.src.InterfaceCli;

import java.io.Serializable;

public class Transfer implements Serializable {

    private String uuid;
    private String cidade;
    private String veiculo;
    private String nPassageiros;
    private String preco;
    private InterfaceCli interfaceCli;

    public Transfer(InterfaceCli interfaceCli, String uuid, String cidade, String veiculo, String nPassageiros, String preco){
        setInterfaceCli(interfaceCli);
        setUuid(uuid);
        setCidade(cidade);
        setVeiculo(veiculo);
        setnPassageiros(nPassageiros);
        setPreco(preco);
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getVeiculo() {
        return veiculo;
    }

    public void setVeiculo(String veiculo) {
        this.veiculo = veiculo;
    }

    public String getnPassageiros() {
        return nPassageiros;
    }

    public void setnPassageiros(String nPassageiros) {
        this.nPassageiros = nPassageiros;
    }

    public String getPreco() {
        return preco;
    }

    public void setPreco(String preco) {
        this.preco = preco;
    }

    public InterfaceCli getInterfaceCli() {
        return interfaceCli;
    }

    public void setInterfaceCli(InterfaceCli interfaceCli) {
        this.interfaceCli = interfaceCli;
    }
}
