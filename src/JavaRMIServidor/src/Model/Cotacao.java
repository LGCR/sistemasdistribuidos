package JavaRMIServidor.src.Model;

import JavaRMICliente.src.InterfaceCli;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

public class Cotacao implements Serializable {

    private String uuid;
    private String CotacaoUuid;
    private String origem;
    private String destino;
    private Date date;
    private String veiculo;
    private String nPassageiros;
    private String precoProposto;
    private InterfaceCli interfaceCli;

    public Cotacao(InterfaceCli interfaceCli, String uuid, String origem, String destino, Date date, String veiculo, String nPassageiros,
                   String precoProposto){
        setInterfaceCli(interfaceCli);
        setUuid(uuid);
        setOrigem(origem);
        setDestino(destino);
        setDate(date);
        setVeiculo(veiculo);
        setnPassageiros(nPassageiros);
        setPrecoProposto(precoProposto);
        setCotacaoUuid(UUID.randomUUID().toString());
    }

    public String getCotacaoUuid() {
        return CotacaoUuid;
    }

    public void setCotacaoUuid(String cotacaoUuid) {
        CotacaoUuid = cotacaoUuid;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public InterfaceCli getInterfaceCli() {
        return interfaceCli;
    }

    public void setInterfaceCli(InterfaceCli interfaceCli) {
        this.interfaceCli = interfaceCli;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getVeiculo() {
        return veiculo;
    }

    public void setVeiculo(String veiculo) {
        this.veiculo = veiculo;
    }

    public String getnPassageiros() {
        return nPassageiros;
    }

    public void setnPassageiros(String nPassageiros) {
        this.nPassageiros = nPassageiros;
    }

    public String getPrecoProposto() {
        return precoProposto;
    }

    public void setPrecoProposto(String precoProposto) {
        this.precoProposto = precoProposto;
    }
}
