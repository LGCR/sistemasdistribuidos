package JavaRMIServidor.src.Model;

import java.io.Serializable;
import java.util.UUID;

public class Proposta implements Serializable {

    private String uuid;
    private Cotacao cotacao;
    private Transfer transfer;
    //Atributo que define se a proposta está firmada entre as duas partes, pendente ou rejeitada.
    //0 = rejeitada, 1 = firmada, 2 = pendente
    private int tipo;
    //Este atributo serve para dizer, quando for 1, que a notificação é de cotacao para transfer.
    // E quando for 2 a notificação é transfer para cotação
    private int relacao;
    private String precoAcordado;
    private String precoProposto;

    public Proposta(Cotacao cotacao, Transfer transfer, int tipo, int relacao){
        setCotacao(cotacao);
        setTransfer(transfer);
        setTipo(tipo);
        setRelacao(relacao);
        setPrecoProposto(cotacao.getPrecoProposto());
        setUuid(UUID.randomUUID().toString());
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getPrecoProposto() {
        return precoProposto;
    }

    public void setPrecoProposto(String precoProposto) {
        this.precoProposto = precoProposto;
    }

    public String getPrecoAcordado() {
        return precoAcordado;
    }

    public void setPrecoAcordado(String precoAcordado) {
        this.precoAcordado = precoAcordado;
    }

    public int getRelacao() {
        return relacao;
    }

    public void setRelacao(int relacao) {
        this.relacao = relacao;
    }

    public Cotacao getCotacao() {
        return cotacao;
    }

    public void setCotacao(Cotacao cotacao) {
        this.cotacao = cotacao;
    }

    public Transfer getTransfer() {
        return transfer;
    }

    public void setTransfer(Transfer transfer) {
        this.transfer = transfer;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }
}
