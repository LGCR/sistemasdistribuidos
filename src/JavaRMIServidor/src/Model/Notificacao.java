package JavaRMIServidor.src.Model;

import java.io.Serializable;

public class Notificacao implements Serializable {

    private String texto;
    private Transfer transfer;
    private Cotacao cotacao;
    //Este atributo serve para dizer, quando for 1, que a notificação é de cotacao para transfer.
    // E quando for 2 a notificação é transfer para cotação. 0 para ambos
    private int relacao;


    public Notificacao(String texto, Transfer transfer, Cotacao cotacao, int relacao){
        setTexto(texto);
        setTransfer(transfer);
        setCotacao(cotacao);
        setRelacao(relacao);
    }

    public int getRelacao() {
        return relacao;
    }

    public void setRelacao(int relacao) {
        this.relacao = relacao;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Transfer getTransfer() {
        return transfer;
    }

    public void setTransfer(Transfer transfer) {
        this.transfer = transfer;
    }

    public Cotacao getCotacao() {
        return cotacao;
    }

    public void setCotacao(Cotacao cotacao) {
        this.cotacao = cotacao;
    }
}
