package JavaRMIServidor.src;

import JavaRMICliente.src.InterfaceCli;
import JavaRMIServidor.src.Model.Cotacao;
import JavaRMIServidor.src.Model.Notificacao;
import JavaRMIServidor.src.Model.Proposta;
import JavaRMIServidor.src.Model.Transfer;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;

public class ServImpl extends UnicastRemoteObject implements InterfaceServ {

    private Evento evento;

    protected ServImpl() throws RemoteException {
        super();
        evento = new Evento();
        new Timer().schedule(evento, 1000, 1000);
    }

    /**
     * Método que lista o menu de ações disponíveis no servidor e invoca o método
     * de impressão na tela do cliente através de referência do cliente.
     * @param interfaceCli Referência do cliente
     */

    @Override
    public void menu(InterfaceCli interfaceCli) {
        try {
            interfaceCli.menu("======================================"
            + "\n1 - Cotações"
            + "\n2 - Cadastro  e alteração de transfer"
            + "\n3 - Reservas"
            + "\n4 - Propostas"
            + "\n5 - Notificações"
            + "\n6 - Listar Transfers"
            + "\n7 - Sair"
            + "\n\n Escolha:");
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * Método que recebe o que o cliente digitou e chama o respectivo método no cliente
     * @param interfaceCli Referência do cliente
     * @param uuid id única do cliente
     * @param op opção que o cliente escolheu
     * @throws RemoteException
     */

    @Override
    public void menuOp(InterfaceCli interfaceCli, String uuid, int op) throws RemoteException {
        switch (op) {
            case 1:
                interfaceCli.menuCotacao("=====================================" +
                    "\nPreencha os campos da Cotação" +
                    "\nExemplo: Origem, Destino, Data, Horário, Tipo de Veículo, Número de Passageiros, Preço Proposto"
                    +"\nExemplo: Curitiba, Orlando, 27/05/19, 15:00, Economia, 3, 1500"
                    + "\n Tipos de Veículos: Economia = até 3 pessoas, Conforto = até 3 pessoas, " +
                        "\nNegócio = até 3 pessoas, Prêmio = até 3 pessoas, VIP = até 3 pessoas, " +
                        "\nSUV = até 5 pessoas, Furgão = até 8 pessoas, Mini Ônibus = até 16 pessoas, " +
                        "\nÔnibus = até 50 pessoas, Helicóptero = até 5 pessoas");
                break;
            case 2:
                if (evento.transferExiste(uuid)){
                    Transfer transfer = evento.getTransfer(uuid);
                    interfaceCli.menuTransfer("Altere algum dos campos" +
                            "\nPara alterar veículo por exemplo, digite => Veículo:Economia"
                            + "\n Para Sair digite => :q", "ALTERACAO");
                } else {
                    interfaceCli.menuTransfer("Insira todos os campos no mesmo formato como o exemplo abaixo" +
                            "\nCampos: Cidade, Tipo do Veículo, Capacidade de Passageiros, Preço" +
                            "\nExemplo: Curitiba, VIP, 3, 1500","CADASTRO");
                }
                break;
            case 3:
                ArrayList<Proposta> reservas = evento.getReservas();
                for (Proposta reserva : reservas){
                    if (reserva.getTipo() == 1) {
                        if (reserva.getCotacao().getUuid().equals(uuid) || reserva.getTransfer().getUuid().equals(uuid)){
                            echoPropostaReservaList(interfaceCli, reserva, reservas);
                        }
                    }
                    interfaceCli.menuPadrao("\n\nCancelar = c + indice da proposta (Exemplo: a5)" , 3);
                }
                break;
            case 4:
                ArrayList<Proposta> propostas = evento.getPropostas();
                for (Proposta proposta : propostas){
                    if (proposta.getTipo() == 2) {
                        if (proposta.getTransfer().getUuid().equals(uuid) && proposta.getRelacao() == 1) {
                            echoPropostaReservaList(interfaceCli, proposta, propostas);
                        } else if (proposta.getRelacao() == 2 && proposta.getCotacao().getUuid().equals(uuid)) {
                            echoPropostaReservaList(interfaceCli, proposta, propostas);
                        }
                    }
                }
                interfaceCli.menuPadrao("\n\nAceitar = a + indice da proposta (Exemplo: a5)" +
                        "\nRejeitar = r+indice"
                        +"\nEditar = e+indice", 4);
                break;
            case 5:
                ArrayList<Notificacao> notificacoes = evento.getNotificacoes();
                for (Notificacao notificacao : notificacoes){
                    if (notificacao.getCotacao().getUuid().equals(uuid) || notificacao.getTransfer().getUuid().equals(uuid)){
                        interfaceCli.echo("\n" + notificacao.getTexto());
                    }
                }
                interfaceCli.menuNotificacao("\nPara sair digite :q");
                break;
            case 6:
                ArrayList<Transfer> transfers = evento.getTransfers();
                String filtro = interfaceCli.scannerListaTransfer("\nDigite a cidade destino");
                for (Transfer transfer : transfers){
                    if (transfer.getCidade().equals(filtro)){
                        interfaceCli.echo("Indice: " + transfers.indexOf(transfer) +
                                "\nTransfer: " + transfer.getUuid()
                        + "\n Cidade: " + transfer.getCidade()
                        + "\n Veículo: " + transfer.getVeiculo()
                        + "\n Capacidade: " + transfer.getnPassageiros()
                        + "\n Preço: " + transfer.getPreco());
                    }
                }
                interfaceCli.menuPadrao("\n\nReservar = rs + indice da proposta (Exemplo: rs5)", 6);
                break;
            case 7:
                evento.clienteSaiu(uuid);
                break;
        }
    }

    /**
     * Método que imprime lista de proposta ou reserva
     * @param interfaceCli
     * @param proposta
     * @param propostas
     */

    private void echoPropostaReservaList(InterfaceCli interfaceCli, Proposta proposta, ArrayList<Proposta> propostas) {
        try {
            interfaceCli.echo("\nIndice: " + propostas.indexOf(proposta)
                    + "\nCliente: " + proposta.getCotacao().getUuid()
                    + "\nTransfer: " + proposta.getTransfer().getUuid()
                    + "\nOrigem, Destino, Data e Horário, Tipo de Veículo, Número de Passageiros, Preço Proposto"
                    + "\n" + proposta.getCotacao().getOrigem()
                    + ", " + proposta.getCotacao().getDestino()
                    + ", " + proposta.getCotacao().getDate().toString()
                    + ", " + proposta.getCotacao().getVeiculo()
                    + ", " + proposta.getCotacao().getnPassageiros()
                    + ", " + proposta.getPrecoProposto());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * Método que recebe os dados que o cliente digitou e cria uma nova cotação
     * @param interfaceCli Referência do Cliente
     * @param cotacao Classe de Dado que representa uma cotação
     * @param uuid id única do cliente
     * @throws RemoteException
     */

    @Override
    public void adicionaCotacao(InterfaceCli interfaceCli, String[] cotacao, String uuid) throws RemoteException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        try {
            Date date = simpleDateFormat.parse((cotacao[2] + " " + cotacao[3]));
            Cotacao cotacao1 = new Cotacao(interfaceCli, uuid.toLowerCase(), cotacao[0].toLowerCase(),
                    cotacao[1].toLowerCase(), date, cotacao[4].toLowerCase(), cotacao[5].toLowerCase(),
                    cotacao[6].toLowerCase());
            evento.adicionaCotacao(cotacao1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * Método que recebe os dados do cliente e cria um novo transfer
     * @param interfaceCli referencia do cliente
     * @param transfer classe de dado que representa um transfer
     * @param uuid id unica do transfer
     * @throws RemoteException
     */

    @Override
    public void adicionaTransfer(InterfaceCli interfaceCli, String[] transfer, String uuid) throws RemoteException {
        Transfer transfer1 = new Transfer(interfaceCli, uuid, transfer[0], transfer[1], transfer[2],
                transfer[3]);
        evento.adicionaTransfer(transfer1);
    }

    /**
     * Método que retorna um transfer para o cliente a partir do id
     * @param uuid id unico do transfer
     * @return Classe de Dado que representa um transfer
     * @throws RemoteException
     */

    @Override
    public Transfer getTransfer(String uuid) throws RemoteException {
        return evento.getTransfer(uuid);
    }

    /**
     * Método que atualiza valores de um transfer recebidos do cliente
     * @param transfer Classe de Dados que representa um transfer
     * @throws RemoteException
     */

    @Override
    public void atualizaTransfer(Transfer transfer) throws RemoteException {
        evento.atualizaTransfer(transfer);
    }

    /**
     * método que trata alguns comandos que alguns menus tem em comum
     * @param acao atributo que contem o que foi digitado pelo cliente
     * @param i indice do menu
     * @param interfaceCli
     * @throws RemoteException
     */

    @Override
    public void menuPadrao(String acao, int i, InterfaceCli interfaceCli) throws RemoteException {
        int index = 0;
        System.out.println(acao);
        if (acao.contains("a")){
            index = Integer.parseInt(acao.substring(1));
            if (i == 4){
                Proposta proposta = evento.getPropostas().get(index);
                proposta.setTipo(1);
                proposta.setPrecoAcordado(proposta.getPrecoProposto());
                evento.adicionaReserva(proposta);
                String texto = "Proposta Aceita " + proposta.getCotacao().getCotacaoUuid()
                        + " reservou seus serviços para a data " + proposta.getCotacao().getDate()
                        + ". Veja mais em Reservas.";
                    evento.notificaTransfer(proposta, texto);
                    evento.notificaCotacao(proposta, texto);
            }
        } else if (acao.contains("rs")){
            if (i == 6) {
                index = Integer.parseInt(acao.substring(2));
                Transfer transfer = evento.getTransfers().get(index);
                interfaceCli.reservar("Digite a origem, data e horário, e número de passageiros separados por vírgula e um espaço", transfer);
            }
        } else if (acao.contains("r")){
            index = Integer.parseInt(acao.substring(1));
            if (i == 4){
                Proposta proposta = evento.getPropostas().get(index);
                proposta.setTipo(0);
                String texto = "Proposta " + proposta.getUuid() + " Rejeitada";
                if (proposta.getRelacao() == 1){
                    proposta.getCotacao().getInterfaceCli().echo(texto);
                    evento.notificaTransfer(proposta, texto);
                } else {
                    proposta.getTransfer().getInterfaceCli().echo(texto);
                    evento.notificaCotacao(proposta, texto);
                }
            }
        }
        else if (acao.contains("c")){
            index = Integer.parseInt(acao.substring(1));
            if (i == 3){
                Proposta reserva = evento.getReservas().get(index);
                reserva.setTipo(0);
                String texto = "A reserva " + reserva.getUuid() + " foi Cancelada";
                evento.notificaTransfer(reserva, texto);
                evento.notificaCotacao(reserva, texto);
            }
        }
        else if (acao.contains("e")){
            if (i == 4){
                index = Integer.parseInt(acao.substring(1));
                Proposta proposta = evento.getPropostas().get(index);
                interfaceCli.editaProposta("Digite os Campos a serem editados e seus valores. Exemplo-> Preço:Curitiba",
                        proposta);
            }
        }
    }

    /**
     * Método que Adiciona Reserva
     * @throws RemoteException
     */

    @Override
    public void adicionaReserva() {
        ArrayList<Proposta> reservas = evento.getPropostas();
        Proposta reserva = reservas.get((reservas.size()-1));
        reserva.setTipo(1);
        evento.adicionaReserva(reserva);
        try {
            reserva.getTransfer().getInterfaceCli().echo("O Cliente " + reserva.getCotacao().getUuid()
                    + " reservou seus serviços para a data " + reserva.getCotacao().getDate()
                    + ". Veja mais em Reservas.");
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * Método que atualiza uma proposta existente
     * @param proposta Classe de Dados que representa uma proposta
     * @throws RemoteException
     */

    @Override
    public void atualizaProposta(Proposta proposta) {
        evento.atualizaProposta(proposta);
    }
}
