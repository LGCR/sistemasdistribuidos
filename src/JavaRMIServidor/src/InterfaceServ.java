package JavaRMIServidor.src;

import JavaRMICliente.src.InterfaceCli;
import JavaRMIServidor.src.Model.Proposta;
import JavaRMIServidor.src.Model.Transfer;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface InterfaceServ extends Remote {

    /**
     * Método que lista o menu de ações disponíveis no servidor e invoca o método
     * de impressão na tela do cliente através de referência do cliente.
     * @param interfaceCli Referência do cliente
     */

    void menu(InterfaceCli interfaceCli) throws RemoteException;

    /**
     * Método que recebe o que o cliente digitou e chama o respectivo método no cliente
     * @param interfaceCli Referência do cliente
     * @param uuid id única do cliente
     * @param op opção que o cliente escolheu
     * @throws RemoteException
     */

    void menuOp(InterfaceCli interfaceCli, String uuid, int op) throws RemoteException;

    /**
     * Método que recebe os dados que o cliente digitou e cria uma nova cotação
     * @param interfaceCli Referência do Cliente
     * @param cotacao Classe de Dado que representa uma cotação
     * @param uuid id única do cliente
     * @throws RemoteException
     */

    void adicionaCotacao(InterfaceCli interfaceCli, String cotacao[], String uuid) throws RemoteException;

    /**
     * Método que recebe os dados do cliente e cria um novo transfer
     * @param interfaceCli referencia do cliente
     * @param transfer classe de dado que representa um transfer
     * @param uuid id unica do transfer
     * @throws RemoteException
     */

    void adicionaTransfer(InterfaceCli interfaceCli, String transfer[], String uuid) throws RemoteException;

    /**
     * Método que retorna um transfer para o cliente a partir do id
     * @param uuid id unico do transfer
     * @return Classe de Dado que representa um transfer
     * @throws RemoteException
     */

    Transfer getTransfer(String uuid) throws RemoteException;

    /**
     * Método que atualiza valores de um transfer recebidos do cliente
     * @param transfer Classe de Dados que representa um transfer
     * @throws RemoteException
     */

    void atualizaTransfer(Transfer transfer) throws RemoteException;

    /**
     * método que trata alguns comandos que alguns menus tem em comum
     * @param acao atributo que contem o que foi digitado pelo cliente
     * @param i indice do menu
     * @param interfaceCli
     * @throws RemoteException
     */

    void menuPadrao(String acao, int i, InterfaceCli interfaceCli) throws RemoteException;

    /**
     * Método que Adiciona Reserva
     * @throws RemoteException
     */

    void adicionaReserva() throws RemoteException;

    /**
     * Método que atualiza uma proposta existente
     * @param proposta Classe de Dados que representa uma proposta
     * @throws RemoteException
     */

    void atualizaProposta(Proposta proposta) throws RemoteException;
}
