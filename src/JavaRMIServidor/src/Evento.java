package JavaRMIServidor.src;

import JavaRMIServidor.src.Model.Cotacao;
import JavaRMIServidor.src.Model.Notificacao;
import JavaRMIServidor.src.Model.Proposta;
import JavaRMIServidor.src.Model.Transfer;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class Evento extends TimerTask {

    private ArrayList<Cotacao> cotacoes;
    private ArrayList<Transfer> transfers;
    private ArrayList<Notificacao> notificacoes;
    private ArrayList<Proposta> propostas;
    private ArrayList<Proposta> reservas;

    Evento(){
        cotacoes = new ArrayList<>();
        transfers = new ArrayList<>();
        notificacoes = new ArrayList<>();
        propostas = new ArrayList<>();
        reservas = new ArrayList<>();
    }

    /**
     * Onde a thread verifica se há propostas ainda
     * pendentes que os transfers podem alterar o preço para atrair o cliente
     */

    @Override
    public void run() {
        Date date = new Date();
        for (Proposta proposta : propostas) {
            Cotacao cotacao = proposta.getCotacao();
            if (!transferExiste(cotacao.getUuid())) {
                long diffInMillies = Math.abs(date.getTime() - cotacao.getDate().getTime());
                long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
                if (diff <= 20) {
                    notificaTransfer(cotacao, "Existem cotações que são compatíveis com o seu estilo de transfer, " +
                            "que tal fazer uma proposta com um preço melhor para o cliente " + cotacao.getUuid() + "?");
                }
            }
        }
    }

    /**
     * Método que notifica os transfers compatíveis com as propostas
     * @param cotacao
     * @param texto
     */

    private void notificaTransfer(Cotacao cotacao, String texto) {
        for (Transfer transfer : transfers){
            if (transfer.getVeiculo().equals(cotacao.getVeiculo())
            && Integer.parseInt(cotacao.getnPassageiros()) <= Integer.parseInt(transfer.getnPassageiros())
            && transfer.getCidade().equals(cotacao.getDestino())){
                try {
                    if (!notificacaoExiste(cotacao)) {
                        notificacoes.add(new Notificacao(texto, transfer, cotacao, 1));
                        transfer.getInterfaceCli().echo(texto);
                    }
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Método que o servidor pode chamar para notificar cliente
     * @param proposta
     * @param texto
     */

    void notificaCotacao(Proposta proposta, String texto) {
        try {
            if (!notificacaoExiste(proposta.getCotacao())){
               notificacoes.add(new Notificacao(texto, proposta.getTransfer(), proposta.getCotacao(), 2));
            }
            proposta.getCotacao().getInterfaceCli().echo(texto);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * Método que o servidor pode chamar para notificar o transfer
     * @param proposta
     * @param texto
     */

    void notificaTransfer(Proposta proposta, String texto) {
        try {
            if (!notificacaoExiste(proposta.getCotacao())){
                notificacoes.add(new Notificacao(texto, proposta.getTransfer(), proposta.getCotacao(), 1));
            }
            proposta.getTransfer().getInterfaceCli().echo(texto);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * Método que verifica se já existe notificação no array de notificações
     * @param cotacao
     * @return
     */

    boolean notificacaoExiste(Cotacao cotacao) {
        for (Notificacao notificacao : notificacoes){
            if (notificacao.getCotacao().getCotacaoUuid().equals(cotacao.getCotacaoUuid())){
                return true;
            }
        }
        return false;
    }

    /**
     * Método que adiciona cotação no array
     * @param cotacao
     */

    void adicionaCotacao(Cotacao cotacao){
        if (!cotacoes.contains(cotacao)) {
            cotacoes.add(cotacao);
        }
        for (Transfer transfer : transfers) {
            for (Proposta reserva : reservas){
                if (reserva.getTipo() == 1 && reserva.getTransfer().getUuid().equals(transfer.getUuid())){
                    if (reserva.getCotacao().getDate().compareTo(cotacao.getDate()) == 0){
                        propostas.add(new Proposta(cotacao, transfer, 2, 1));
                    }
                }
            }
            if (reservas.isEmpty()){
                propostas.add(new Proposta(cotacao, transfer, 2, 1));
            }
        }

        try {
            cotacao.getInterfaceCli().echo("Cotação registrada com sucesso");
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    void adicionaProposta(){
        for (Cotacao cotacao : cotacoes){
            adicionaCotacao(cotacao);
        }
    }

    /**
     * Método que adiciona transfer no array
     * @param transfer
     */

    void adicionaTransfer(Transfer transfer){
        transfers.add(transfer);
        if (!cotacoes.isEmpty()){
            adicionaProposta();
        }
        try {
            transfer.getInterfaceCli().echo("Transfer registrado com sucesso");
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * Métooo que verifica se cotação existe
     * @param uuid
     * @return
     */

    boolean cotacaoExiste(String uuid){
        for (Cotacao cotacao : cotacoes){
            if (cotacao.getUuid().equals(uuid)){
                return true;
            }
        }
        return false;
    }

    /**
     * Método que verifica se transfer existe
     * @param uuid
     * @return
     */

    boolean transferExiste(String uuid){
        for (Transfer transfer : transfers){
            if (transfer.getUuid().equals(uuid)){
                return true;
            }
        }
        return false;
    }

    /**
     * Retorna um transfer específico
     * @param uuid
     * @return
     */

    Transfer getTransfer(String uuid){
        for (Transfer transfer : transfers){
            if (transfer.getUuid().equals(uuid)){
                return transfer;
            }
        }
        return null;
    }

    /**
     * Atualiza um transfer no array
     * @param transfer
     */

    void atualizaTransfer(Transfer transfer) {
        for (int i = 0; i < transfers.size(); i++){
            if (transfers.get(i).getUuid().equals(transfer.getUuid())){
                transfers.set(i, transfer);
                try {
                    transfer.getInterfaceCli().echo("Transfer atualizado com sucesso");
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Retorna todas as cotações
     * @return
     */

    public ArrayList<Cotacao> getCotacoes() {
        return cotacoes;
    }

    /**
     * Retorna todos os transfers
     * @return
     */

    public ArrayList<Transfer> getTransfers() {
        return transfers;
    }

    /**
     * Retorna todas as notificações
     * @return
     */

    public ArrayList<Notificacao> getNotificacoes() {
        return notificacoes;
    }

    /**
     * Retorna todas as propostas
     * @return
     */

    public ArrayList<Proposta> getPropostas() {
        return propostas;
    }

    /**
     * Retorna todas as reservas
     * @return
     */

    public ArrayList<Proposta> getReservas() {
        return reservas;
    }

    /**
     * Adiciona reservaa no array
     * @param proposta
     */

    public void adicionaReserva(Proposta proposta) {
        reservas.add(proposta);
    }

    /**
     * Atualiza arrays removendo cliente
     * @param uuid
     */

    public void clienteSaiu(String uuid) {
        retirarCotacao(uuid);
        retirarTransfer(uuid);
    }

    /**
     * Retira cliente da lista de transfer
     * @param uuid
     */

    void retirarTransfer(String uuid){
        for(Transfer transfer : transfers){
            if (transfer.getUuid().equals(uuid)){
                transfers.remove(transfer);
                break;
            }
        }
    }

    /**
     * Retira cliente da lista de cotação
     * @param uuid
     */

    void retirarCotacao(String uuid){
        for(Cotacao cotacao : cotacoes){
            if (cotacao.getUuid().equals(uuid)){
                cotacoes.remove(cotacao);
                break;
            }
        }
    }

    /**
     * Atualiza uma proposta no array
     * @param proposta
     */

    public void atualizaProposta(Proposta proposta) {
        for (Proposta p : propostas){
            if (p.getUuid().equals(proposta.getUuid())){
                propostas.set(propostas.indexOf(p), proposta);
                break;
            }
        }
    }
}
