import React, { Component } from 'react';
import axios from 'axios';
import logo from './logo.svg';
import './App.css';

class ReservaPopup extends Component{

    constructor(props){
        super(props);
        this.state = {
            dataa: "",
            horario: "",
            passageiros: "",
            origem: ""
        };

        this.handleOrigem = this.handleOrigem.bind(this);
        this.handleDataa = this.handleDataa.bind(this);
        this.handleHorario = this.handleHorario.bind(this);
        this.handlePasageiros = this.handlePasageiros.bind(this);
        this.handleReserva = this.handleReserva.bind(this);

    }

    render() {
        return(
            <div className={"popup"}>
                <div className={"popup_inner"}>
                    <form id="sg2" onSubmit={this.props.reservar}>
                        <label>Origem <input name="origem" id="origem" value={this.state.origem} onChange={
                            this.handleOrigem}/></label>
                        <label>Data <input name="dataa" id="dataa" value={this.state.dataa} onChange={
                            this.handleDataa}/></label>
                        <label>Horário <input name="horario" id="horario" value={this.state.horario} onChange={
                            this.handleHorario}/></label>
                        <label>Passageiros <input name="passageiros" id="passageiros" value={this.state.passageiros}
                                                 onChange={this.handlePasageiros}/></label>
                        <input type="submit" value="Reservar" onClick={this.handleReserva}/>
                    </form>
                </div>
            </div>
        );
    }

    handleReserva(){
        const reserva = {
          idCliente: this.props.idCliente,
          idTransfer: this.props.idTransfer,
          origem: this.state.origem,
          destino: this.props.destino,
          dataa: this.state.dataa,
          horario: this.state.horario,
          veiculo: this.props.veiculo,
          passageiros: this.state.passageiros,
          preco: this.props.preco
        };

        axios.post(this.props.url + "/reservar", reserva)
            .then(res => {
                console.log(res);
                console.log(res.data);
                if (res.data === 0){
                    console.log("Reserva ok")
                }
            })

    }

    handleOrigem(event){
        this.setState({origem: event.target.value});
    }

    handleDataa(event){
        this.setState({dataa: event.target.value});
    }

    handleHorario(event){
        this.setState({horario: event.target.value});
    }

    handlePasageiros(event){
        this.setState({passageiros: event.target.value});
    }

}

class App extends Component {

    constructor(props){
        super(props);
        this.state = {
            url: "http://localhost:8080/api/v1/",
            veiculos: [],
            cidade : "",
            veiculo : "",
            capacidade : "",
            preco : "",
            mostrarReservas: false,
            escolhaMenu: true,
            mostraTransfer: false,
            transfers: [],
            reservas: [],
            id: "",
            mostraPopup: false,
            origem: ""
        };

        // Aqui utilizamos o `bind` para que o `this` funcione dentro da nossa callback
        this.handleCidade = this.handleCidade.bind(this);
        this.handleVeiculo = this.handleVeiculo.bind(this);
        this.handleCapacidade = this.handleCapacidade.bind(this);
        this.handlePreco = this.handlePreco.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.togglePopup = this.togglePopup.bind(this);
        this.handleAtualiza = this.handleAtualiza.bind(this)
    }

    async componentDidMount() {

        axios.get(this.state.url + "veiculos")
            .then(response => {
                console.log(response.data);
                this.setState({veiculos:response.data})
            });
        axios.get(this.state.url + "transfers")
            .then(response => {
                console.log(response.data);
                this.setState({transfers:response.data})
            });
        axios.get(this.state.url + "reservas")
            .then(response => {
                console.log(response.data);
                this.setState({reservas:response.data})
            })
    }


    render() {
            return (
                <div className="App">
                    <div className="App-header">
                        <img src={logo} className="App-logo" alt="logo"/>
                        <h2>Welcome to React Transfer System</h2>
                    </div>
                    <div className="App-content">
                        <button className={"batualiza"}>Atualizar</button>
                        <table>
                            <tbody>
                                    <th>ID</th>
                                    <th>Cidade</th>
                                    <th>Veículo</th>
                                    <th>Capacidade</th>
                                    <th>Preço</th>

                            </tbody>
                            {this.state.transfers.map((transfer) => {
                                return <tbody>
                                <td>{transfer.uuid}</td>
                                <td>{transfer.cidade}</td>
                                <td>{transfer.veiculo}</td>
                                <td>{transfer.capacidade}</td>
                                <td>{transfer.preco}</td>
                                <td>
                                    <button className={"breserva"} onClick={this.togglePopup}>Reservar</button>
                                </td>
                                {this.state.mostraPopup ?
                                    <ReservaPopup text={"Reservar"} reservar={this.togglePopup} destino={transfer.cidade}
                                    idTransfer={transfer.uuid} idCliente={this.state.id} veiculo={transfer.veiculo}
                                    preco={transfer.preco} url={this.state.url}>
                                    </ReservaPopup> : null
                                }
                                </tbody>
                            })}
                        </table>
                    </div>
                </div>
            );
  }

  togglePopup(){
        this.setState({mostraPopup: !this.state.mostraPopup});
  }

    handleMostrarClick() {
        this.setState(state => ({
            cadastrarTransfer: false,
            escolhaMenu: false
        }));
    }

    handleCadastrarClick() {
        const id = {
            id: this.state.id
        };
        axios.post(this.state.url + "/verificaTransfer", id)
            .then(response => {
                console.log(response.data);
               if (response.data === true){
                   this.setState(state => ({
                       mostraTransfer: true,
                       escolhaMenu: false,
                       mostrarReservas:true
                   }));
               } else {
                   this.setState(state => ({
                       mostraTransfer: true,
                       escolhaMenu: false,
                       mostrarReservas:false
                   }));
               }
            });
    }

  handleCidade(event) {
      this.setState({cidade: event.target.value});
  }

    handleVeiculo(event) {
        this.setState({veiculo: event.target.value});
    }

    handleCapacidade(event) {
        this.setState({capacidade: event.target.value});
    }

    handlePreco(event) {
        this.setState({preco: event.target.value});
    }

  handleSubmit = event => {
      event.preventDefault();

      const transfer = {
          id: this.state.id,
          cidade : this.state.cidade,
          veiculo : this.state.veiculo,
          capacidade : this.state.capacidade,
          preco : this.state.preco
      };

        axios.post(this.state.url + "/adiciona-transfer", transfer)
            .then(res => {
                console.log(res);
                console.log(res.data);
                if (res.data === 0){
                    this.setState({mostrarReservas:true});
                }
            })
  }
    handleAtualiza(){
        axios.get(this.state.url + "reservas")
            .then(response => {
                console.log(response.data);
                this.setState({reservas:response.data})
            });
    }

}

export default App;
