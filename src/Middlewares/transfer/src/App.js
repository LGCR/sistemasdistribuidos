import React, { Component } from 'react';
import axios from 'axios'
import logo from './logo.svg';
import './App.css';

class App extends Component {

    constructor(props){
        super(props);
        this.state = {
            url: "http://localhost:8080/api/v1/",
            veiculos: [],
            cidade : "",
            veiculo : "",
            capacidade : "",
            preco : "",
            mostrarReservas: false,
            transfers: [],
            reservas: [],
            id: ""
        };

        // Aqui utilizamos o `bind` para que o `this` funcione dentro da nossa callback
        this.handleCidade = this.handleCidade.bind(this);
        this.handleVeiculo = this.handleVeiculo.bind(this);
        this.handleCapacidade = this.handleCapacidade.bind(this);
        this.handlePreco = this.handlePreco.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleAtualiza = this.handleAtualiza.bind(this);
    }

    async componentDidMount() {
        const head = {
            headers: { 'Content-Type': 'text/plain;charset=UTF-8'
            }
        };
        this.setState({id:window.location.href});

        axios.get(this.state.url + "veiculos", head)
            .then(response => {
                console.log(response.data);
                this.setState({veiculos:response.data})
            });/*
        axios.get(this.state.url + "transfers", head)
            .then(response => {
                console.log(response.data);
                this.setState({transfers:response.data})
            });*/
        axios.get(this.state.url + "reservas", head)
            .then(response => {
                console.log(response.data);
                this.setState({reservas:response.data})
            });

        const id = {
            id: window.location.href
        };
        axios.post(this.state.url + "/verificaTransfer", id)
            .then(response => {
                console.log(response.data);
                if (response.data === true){
                    this.setState(state => ({
                        mostrarReservas:true
                    }));
                } else {
                    this.setState(state => ({
                        mostrarReservas:false
                    }));
                }
            });
    }

  render() {
      if (!this.state.mostrarReservas) {
          return (
              <div className="App">
                  <div className="App-header">
                      <img src={logo} className="App-logo" alt="logo"/>
                      <h2>Welcome to React Transfer System</h2>
                  </div>
                  <div className="App-content">
                      <form id="sg1" onSubmit={this.handleSubmit}>
                          <label>Cidade <input name="cidade" id="cidade" value={this.state.cidade} onChange={
                              this.handleCidade}/></label>
                          <label>Veículo <select name="veiculo" id="veiculo" value={this.state.veiculo} onChange={
                              this.handleVeiculo}>
                              <option>Selecione o Veículo</option>
                              {this.state.veiculos.map((veiculo, id) => {
                                  return <option key={id} value={veiculo.id}>
                                      {veiculo.veiculo}</option>
                              })}
                          </select></label>
                          <label>Capacidade <input name="capacidade" id="capacidade" value={this.state.capacidade}
                                                   onChange={this.handleCapacidade}/></label>
                          <label>Preço <input name="preco" id="preco" value={this.state.preco} onChange={
                              this.handlePreco}/></label>
                          <input type="submit" value="Registrar"/>
                      </form>
                  </div>
              </div>
          );
      } else {
          return (
              <div className="App">
                  <div className="App-header">
                      <img src={logo} className="App-logo" alt="logo"/>
                      <h2>Welcome to React Transfer System</h2>
                  </div>
                  <div className="App-content">
                      <button className={"batualiza"} onClick={this.handleAtualiza()}>Atualizar</button>
                      <table>
                          <tbody>
                          <th>Cliente</th>
                          <th>Transfer</th>
                          <th>Origem</th>
                          <th>Destino</th>
                          <th>Data</th>
                          <th>Horário</th>
                          <th>Veículo</th>
                          <th>Número de Passageiros</th>
                          <th>Preço</th>
                          </tbody>
                          {this.state.reservas.map((reserva) => {
                              if (reserva.idTransfer === this.state.id) {
                              return <tbody>
                                  <td>{reserva.idCliente}</td>
                                  <td>{reserva.idTransfer}</td>
                                  <td>{reserva.origem}</td>
                                  <td>{reserva.destino}</td>
                                  <td>{reserva.dataa}</td>
                                  <td>{reserva.horario}</td>
                                  <td>{reserva.veiculo}</td>
                                  <td>{reserva.passageiros}</td>
                                  <td>{reserva.preco}</td>
                                  </tbody>
                              }
                          })}
                      </table>
                  </div>
              </div>
          );
      }
  }

    handleCidade(event) {
        this.setState({cidade: event.target.value});
    }

    handleVeiculo(event) {
        this.setState({veiculo: event.target.value});
    }

    handleCapacidade(event) {
        this.setState({capacidade: event.target.value});
    }

    handlePreco(event) {
        this.setState({preco: event.target.value});
    }

    handleSubmit = event => {
        event.preventDefault();

        const transfer = {
            id: this.state.id,
            cidade : this.state.cidade,
            veiculo : this.state.veiculo,
            capacidade : this.state.capacidade,
            preco : this.state.preco
        };

        axios.post(this.state.url + "/adiciona-transfer", transfer)
            .then(res => {
                console.log(res);
                console.log(res.data);
                if (res.data === 0){
                    this.setState({mostrarReservas:true});
                }
            })
    };

    handleAtualiza(){
        axios.get(this.state.url + "reservas")
            .then(response => {
                console.log(response.data);
                this.setState({reservas:response.data})
            });
    }

}

export default App;
