package sistemas.distribuidos.middleware;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;

@RestController

@RequestMapping("/api/v1")

public class RestControle {

    private ArrayList<Veiculo> veiculos = new ArrayList<>();
    private ArrayList<Transfer> transfers = new ArrayList<>();
    private ArrayList<Reserva> reservas = new ArrayList<>();

    RestControle(){
        insereVeiculos();
    }

    private void insereVeiculos() {
        veiculos.add(new Veiculo("Economia", 3));
        veiculos.add(new Veiculo("Conforto", 3));
        veiculos.add(new Veiculo("Negócio", 3));
        veiculos.add(new Veiculo("Prêmio", 3));
        veiculos.add(new Veiculo("VIP", 3));
        veiculos.add(new Veiculo("SUV", 5));
        veiculos.add(new Veiculo("Furgão", 8));
        veiculos.add(new Veiculo("Mini Ônibus", 16));
        veiculos.add(new Veiculo("Ônibus", 50));
        veiculos.add(new Veiculo("Helicóptero", 5));
    }

    @CrossOrigin(origins = "https://localhost:3000")
    @GetMapping("/veiculos")
    public ArrayList<Veiculo> veiculos() {
        return veiculos;
    }

    @GetMapping("/transfers")
    public ArrayList<Transfer> transfers(){
        return transfers;
    }

    @GetMapping("/reservas")
    public ArrayList<Reserva> reservas(){
        return reservas;
    }

    @PostMapping("/adiciona-transfer")
    public int adicionaTransfer(@RequestBody JsonNode transfer){
        try {
            transfers.add(new Transfer(transfer.get("id").textValue(),
                    transfer.get("cidade").textValue(), transfer.get("veiculo").textValue(),
                    transfer.get("capacidade").textValue(), transfer.get("preco").textValue()));
            return 0;
        }catch (Exception e){
            return 1;
        }
    }

    @PostMapping("/verificaTransfer")
    public boolean verificaTransfer(@RequestBody JsonNode id){
        for (Transfer transfer : transfers){
            if (transfer.getUuid().equals(id.get("id").textValue())){
                return true;
            }
        }
        return false;
    }

    @PostMapping("/reservar")
    public int adicionaReserva(@RequestBody JsonNode reserva){
        try {
            System.out.println(reserva);
            reservas.add(new Reserva(reserva.get("idCliente").textValue(), reserva.get("idTransfer").textValue(),
                    reserva.get("origem").textValue(), reserva.get("destino").textValue(),
                    reserva.get("dataa").textValue(), reserva.get("horario").textValue(),
                    reserva.get("veiculo").textValue(), reserva.get("passageiros").textValue(),
                    reserva.get("preco").textValue()));
            return 0;
        }catch (Exception e){
            e.printStackTrace();
            return 1;
        }
    }
}
