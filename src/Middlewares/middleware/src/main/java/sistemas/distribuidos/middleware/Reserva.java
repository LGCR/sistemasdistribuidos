package sistemas.distribuidos.middleware;

import java.io.Serializable;
import java.util.UUID;

public class Reserva implements Serializable {

    private String uuid;
    private String idCliente;
    private String idTransfer;
    private String origem;
    private String destino;
    private String data;
    private String horario;
    private String veiculo;
    private String passageiros;
    private String preco;

    Reserva(String idCliente, String idTransfer, String origem,
            String destino, String data, String horario, String veiculo, String passageiros, String preco){
        setUuid(UUID.randomUUID().toString());
        setIdCliente(idCliente);
        setIdTransfer(idTransfer);
        setOrigem(origem);
        setDestino(destino);
        setData(data);
        setHorario(horario);
        setVeiculo(veiculo);
        setPassageiros(passageiros);
        setPreco(preco);
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getIdTransfer() {
        return idTransfer;
    }

    public void setIdTransfer(String idTransfer) {
        this.idTransfer = idTransfer;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public String getVeiculo() {
        return veiculo;
    }

    public void setVeiculo(String veiculo) {
        this.veiculo = veiculo;
    }

    public String getPassageiros() {
        return passageiros;
    }

    public void setPassageiros(String passageiros) {
        this.passageiros = passageiros;
    }

    public String getPreco() {
        return preco;
    }

    public void setPreco(String preco) {
        this.preco = preco;
    }
}
