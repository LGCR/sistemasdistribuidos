package sistemas.distribuidos.middleware;

public class Veiculo {

    private String veiculo;
    private int capacidade;

    Veiculo(String veiculo, int capacidade){
        setVeiculo(veiculo);
        setCapacidade(capacidade);
    }

    public String getVeiculo() {
        return veiculo;
    }

    public void setVeiculo(String veiculo) {
        this.veiculo = veiculo;
    }

    public int getCapacidade() {
        return capacidade;
    }

    public void setCapacidade(int capacidade) {
        this.capacidade = capacidade;
    }
}
