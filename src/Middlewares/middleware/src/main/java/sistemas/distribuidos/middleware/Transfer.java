package sistemas.distribuidos.middleware;

import java.io.Serializable;

public class Transfer implements Serializable {

    private String uuid;
    private String cidade;
    private String veiculo;
    private String capacidade;
    private String preco;

    Transfer(String uuid, String cidade, String veiculo, String capacidade, String preco){
        setUuid(uuid);
        setCidade(cidade);
        setVeiculo(veiculo);
        setCapacidade(capacidade);
        setPreco(preco);
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getVeiculo() {
        return veiculo;
    }

    public void setVeiculo(String veiculo) {
        this.veiculo = veiculo;
    }

    public String getCapacidade() {
        return capacidade;
    }

    public void setCapacidade(String capacidade) {
        this.capacidade = capacidade;
    }

    public String getPreco() {
        return preco;
    }

    public void setPreco(String preco) {
        this.preco = preco;
    }
}
