package sistemas.distribuidos.middleware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class MiddlewareApplication {

	private String pathPadrao = "/api/v1/";
	private String origin = "http://localhost:3000";

	public static void main(String[] args) {
		SpringApplication.run(MiddlewareApplication.class, args);
		RestControle restControle = new RestControle();
	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping(pathPadrao + "veiculos").allowedOrigins("*");
				registry.addMapping(pathPadrao + "adiciona-transfer").allowedOrigins("*");
                registry.addMapping(pathPadrao + "transfers").allowedOrigins("*");
                registry.addMapping(pathPadrao + "reservas").allowedOrigins("*");
                registry.addMapping(pathPadrao + "reservar").allowedOrigins("*");
                registry.addMapping(pathPadrao + "verificaTransfer").allowedOrigins("*");
			}
		};
	}

}
