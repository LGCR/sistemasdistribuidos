package JavaRMICliente.src;

import JavaRMIServidor.src.InterfaceServ;
import JavaRMIServidor.src.Model.Proposta;
import JavaRMIServidor.src.Model.Transfer;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;
import java.util.UUID;

public class CliImpl extends UnicastRemoteObject implements InterfaceCli {

    private InterfaceServ serv;
    static UUID uuid;

    protected CliImpl(InterfaceServ serv) throws RemoteException {
        super();
        this.serv = serv;
        uuid = UUID.randomUUID();
        this.serv.menu(this);
    }

    /**
     * Método que imprime textos recebidos pelo servidor na tela do cliente.
     * @param texto
     */

    @Override
    public void echo(String texto) {
        System.out.println(texto);
    }

    /**
     * Método que trata das opções do menu principal
     * @param texto
     * @throws RemoteException
     */

    @Override
    public void menu(String texto) throws RemoteException {
        String op = "";
        while(!op.equals("1") && !op.equals("2") && !op.equals("3") && !op.equals("4")
                && !op.equals("5") && !op.equals("6") && !op.equals("7")) {
            echo(texto);
            Scanner scanner = new Scanner(System.in);
            op = scanner.nextLine();
            serv.menuOp(this, uuid.toString(), Integer.parseInt(op));
            if (op.equals("7")){
                System.exit(0);
            }
        }
    }

    /**
     * Método que recebe o que o cliente digitou e envia ao servidor para que seja criada uma cotação
     * @param texto
     * @throws RemoteException
     */

    @Override
    public void menuCotacao(String texto) throws RemoteException {
        echo(texto);
        Scanner scanner = new Scanner(System.in);
        String cotacao = scanner.nextLine().toLowerCase();
        String[] cotacaoArray = cotacao.split(", ");
        if (cotacaoArray.length == 7){
            serv.adicionaCotacao(this, cotacaoArray, uuid.toString());
        } else {
            serv.menuOp(this, uuid.toString(), 1);
        }
        serv.menu(this);
    }

    /**
     * Método que recebe o que o cliente digitou e envia ao servidor para que seja criado um transfer.
     * Se cliente já é um transfer, o método trata quais valores o cliente deseja alterar no transfer.
     * @param texto
     * @param tipo
     * @throws RemoteException
     */

    @Override
    public void menuTransfer(String texto, String tipo) throws RemoteException {
        echo(texto);
        Scanner scanner = new Scanner(System.in);
        String acao = "";
        if (tipo.equals("ALTERACAO")){
            while (!acao.equals(":q")){
                acao = scanner.nextLine().toLowerCase();
                if (!acao.equals(":q")) {
                    String[] campos = acao.split("[: ]");
                    Transfer transfer = serv.getTransfer(uuid.toString());
                    for (int i = 0; i < campos.length; i++) {
                        if (campos[i].equals("veiculo") || campos[i].equals("veículo")) {
                            transfer.setVeiculo(campos[i + 1]);
                        } else if (campos[i].equals("capacidade") || campos[i].equals("passageiros")) {
                            transfer.setnPassageiros(campos[i + 1]);
                        } else if (campos[i].equals("preco") || campos[i].equals("preço") || campos[i].equals("valor")) {
                            transfer.setPreco(campos[i + 1]);
                        } else if (campos[i].equals("cidade")){
                            transfer.setCidade(campos[i+1]);
                        }
                    }
                    serv.atualizaTransfer(transfer);
                }
            }
        } else {
            acao = scanner.nextLine();
            String[] transfer = acao.toLowerCase().split(", ");
            if (transfer.length == 4) {
                serv.adicionaTransfer(this, transfer, uuid.toString());
            } else {
                serv.menuOp(this, uuid.toString(), 2);
            }
        }
        serv.menu(this);
    }

    /**
     * Método que edita uma proposta feita a um dos lados e reenvia à outra ponta
     * @param texto
     * @param proposta
     * @throws RemoteException
     */

    @Override
    public void editaProposta(String texto, Proposta proposta) {
        echo(texto);
        Scanner scanner = new Scanner(System.in);
        String acao = "";
        while (!acao.equals(":q")) {
            if (!acao.equals("")) {
                String[] campos = acao.split("[: ]");
                for (int i = 0; i < campos.length; i++) {
                    if (campos[i].equals("preco") || campos[i].equals("preço") || campos[i].equals("valor")) {
                        proposta.setPrecoProposto(campos[i + 1]);
                    }
                }
                break;
            }
            acao = scanner.nextLine().toLowerCase();
        }
        try {
            if (proposta.getRelacao() == 1){
                proposta.setRelacao(2);
            } else {
                proposta.setRelacao(1);
            }
            serv.atualizaProposta(proposta);
            serv.menu(this);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * Método que envia comando ao servidor sobre o que o cliente quer fazer nos menus de proposta e reserva.
     * @param texto
     * @param i
     * @throws RemoteException
     */

        @Override
    public void menuPadrao(String texto, int i) throws RemoteException {
        echo(texto);
        Scanner scanner = new Scanner(System.in);

        String acao = "";
        while (!acao.equals(":q")){
            acao = scanner.nextLine().toLowerCase();
            serv.menuPadrao(acao, i, this);
        }
        serv.menu(this);
    }

    /**
     * Método que envia o comando ao servidor sobre o que o cliente quer fazer no menu de notificação.
     * @param texto
     * @throws RemoteException
     */

    @Override
    public void menuNotificacao(String texto) throws RemoteException {
        echo(texto);
        Scanner scanner = new Scanner(System.in);
        String acao = "";
        while (!acao.equals(":q")){
            acao = scanner.nextLine();
        }
        serv.menu(this);
    }

    /**
     * Método que pega o que o cliente digitou no filtro para listar o transfer
     * @param texto
     * @return
     * @throws RemoteException
     */

    @Override
    public String scannerListaTransfer(String texto) throws RemoteException {
        echo(texto);
        Scanner scanner = new Scanner(System.in);
        return (scanner.nextLine().toLowerCase());
    }

    /**
     * Método que pega as informações complementares para reservar um transfer pelo menu de listar transfer
     * @param s
     * @param transfer
     * @throws RemoteException
     */

    @Override
    public void reservar(String s, Transfer transfer) {
        echo(s);
        Scanner scanner = new Scanner(System.in);
        String reserva = scanner.nextLine().toLowerCase();
        String[] r = reserva.split(", ");
        try {
            if (r.length == 4) {
                String[] reservaArray = {r[0], transfer.getCidade(), r[1], r[2], transfer.getVeiculo(), r[3], transfer.getPreco()};
                serv.adicionaCotacao(this, reservaArray, uuid.toString());
                serv.adicionaReserva();
            }
            else {
                serv.menuOp(this, uuid.toString(), 3);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
