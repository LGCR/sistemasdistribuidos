package JavaRMICliente.src;

import JavaRMIServidor.src.Model.Proposta;
import JavaRMIServidor.src.Model.Transfer;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface InterfaceCli extends Remote {

    /**
     * Método que imprime textos recebidos pelo servidor na tela do cliente.
     * @param texto
     */

    void echo (String texto) throws RemoteException;

    /**
     * Método que trata das opções do menu principal
     * @param texto
     * @throws RemoteException
     */

    void menu (String texto) throws RemoteException;

    /**
     * Método que recebe o que o cliente digitou e envia ao servidor para que seja criada uma cotação
     * @param texto
     * @throws RemoteException
     */

    void menuCotacao (String texto) throws RemoteException;

    /**
     * Método que recebe o que o cliente digitou e envia ao servidor para que seja criado um transfer.
     * Se cliente já é um transfer, o método trata quais valores o cliente deseja alterar no transfer.
     * @param texto
     * @param tipo
     * @throws RemoteException
     */

    void menuTransfer (String texto, String tipo) throws RemoteException;

    /**
     * Método que envia comando ao servidor sobre o que o cliente quer fazer nos menus de proposta e reserva.
     * @param texto
     * @param i
     * @throws RemoteException
     */

    void menuPadrao(String texto, int i) throws RemoteException;

    /**
     * Método que envia o comando ao servidor sobre o que o cliente quer fazer no menu de notificação.
     * @param texto
     * @throws RemoteException
     */

    void menuNotificacao(String texto) throws RemoteException;

    /**
     * Método que pega o que o cliente digitou no filtro para listar o transfer
     * @param texto
     * @return
     * @throws RemoteException
     */

    String scannerListaTransfer(String texto) throws RemoteException;

    /**
     * Método que pega as informações complementares para reservar um transfer pelo menu de listar transfer
     * @param s
     * @param transfer
     * @throws RemoteException
     */

    void reservar(String s, Transfer transfer) throws RemoteException;

    /**
     * Método que edita uma proposta feita a um dos lados e reenvia à outra ponta
     * @param texto
     * @param proposta
     * @throws RemoteException
     */

    void editaProposta(String texto, Proposta proposta) throws RemoteException;
}
